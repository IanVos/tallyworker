--Global variables
settings_file = "settings.lua"

port = 8124
cam = 0
liveCam = 0
tallyName = ''

function GetIP(sck, msg, port, ip)
  if msg == "ip address" then
    ip_address = ip
    init()
  end
  sck:close()
end

function UDP()
  if ip_address == "0" then
    udpsock = net.createUDPSocket()
    print("Listening for broadcast...")
    if udpsock then
      udpsock:listen(6024)
      udpsock:on("receive", GetIP)
    end
  end
end

function init()
  print("init...")
  -- Set GPIO modes
  gpio.mode(5, gpio.OUTPUT)
  gpio.write(5, gpio.LOW)

  connected = false
    if port ~= 0 then
      --Create a new connection
      print("Creating connection...")
      socket = net.createConnection(net.TCP, 0)
      socket:on("receive", receive) -- Set event on receive (run receive())
      print("Opening connection... (port:" .. port .. ")")
      print(ip_address)
      socket:connect(port, ip_address) --open the connection
      print("connecting")
      socket:on("connection", function(soc, msg)
        print("connected")
        connected = true
      end)
      tmr.delay(500000)
    else
      print("No port!" .. port)
  end
end

--When a message is received
--Display message and proform actions
function receive(connection, msg)
  print("Received: "..msg)
  command = getOnPosition(msg, 1)
  if command == 1 then
    --change live camera
    liveCam = getOnPosition(msg, 2)
    if liveCam == cam then
      --ON
      gpio.write(5, gpio.HIGH)
    else
      --OFF
      gpio.write(5, gpio.LOW)
    end
  elseif command == 2 then
    -- send chip name
    socket:send(node.chipid().."-"..tallyName.."-"..cam)
  elseif command == 3 then
    split = stringSplit(msg, "-")
    cam = split[2]
    tallyName = split[3]
    save_config()
  elseif command == 4 then
    --Forget current wifi network
    wifi.sta.clearconfig()
  end
end

function stringSplit(text, seperator)
  index = 1 -- Yes in lua it starts with 2
  result = {}
  for part in string.gmatch(text, "[^"..seperator.."]+") do
    print(part)
    result[index] = part
    index = index + 1
  end
  return result
end
--Read settings file
function read_settings()
  local settings, e = loadfile(settings_file) --load the settings file (if error settings is nill)
  if settings then
    settings() --inisiate settings
    print("Loaded settings!")
  else
    print("No settings file")
  end
end

--Get a number from a position in a string
function getOnPosition(str, position)
  return tonumber(string.sub(str, position, position))
end

--save cam and tally assignment
function save_config()
  file.open(settings_file, "w")
  file.write("port=".. port .."\n")
  file.write("ip_address='".. ip_address .."'\n")
  file.write("cam=".. cam .."\n")
  file.write("tallyName='".. tallyName .."'\n")
  file.close()
  print("Config saved")
 end

--Start of code
if tally_running then
  print('Tally system already running!')
  return
end
tally_running = true
read_settings()

-- Wifi setup, Wait until a wifi connection is made
print("setting up WiFi")
print("start station ap")
wifi.setmode(wifi.STATIONAP)
wifi.sta.connect()
print("start enduser")
enduser_setup.start(
  function()
    print("Connected to wifi as:" .. wifi.sta.getip())
    UDP()
  end,
  function(error, str)
    print("WIFI setup error: " .. err .. ": " .. str)
  end,
  print
);
